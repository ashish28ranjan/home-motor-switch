# Install python3.5
#
# sudo apt install python3-pip
# sudo pip3 install virtualenv
# virtualenv --python=/usr/bin/python3.5 hms-env
# source /path/to/hms-env/bin/activate

import os
import threading
import signal
import time
import logging
from datetime import datetime

# pip install RPi.GPIO
import RPi.GPIO as GPIO

# pip install python-telegram-bot --upgrade
import telegram
from telegram.error import NetworkError, Unauthorized

from telegramenv import *


DEBUG = 1
LOG = 1

logDir = os.path.expanduser("~") + "/logs/"
logFile = "home_motor_switch.log"

WSpin = 7   # RPi waterSupply pin number (BOARD notation)
WFpin = 11  # RPi waterFilled pin number (BOARD notation)
DOWN_MOTOR_PIN = 15 # Output pin to control the Down Motor through relay

OTFpin = 12 # RPi overheadTankFilled pin number (BOARD notation)
UP_MOTOR_PIN = 18   # Output pin to control the Up Motor through relay

MST = 06.30    # Morning start time (in military hours and military minutes. e.g. 06.30 = 6 hours 30 minutes)
MET = 10.05    # Morning end time (in military hours and military minutes. e.g. 10.05 = 10 hours 5 minutes)
NST = 14.15    # Night start time (in military hours and military minutes. e.g. 19.15 = 19 hours 15 minutes)
NET = 22.20    # Night end time (in military hours and military minutes. e.g. 22.20 = 22 hours 20 minutes)

TRD = 30    # Test run duration (in seconds)
TRI = 1    # Test run interval (in minutes)

MUPD_DOWN = 240 # Maximum usage per day for Down Motor (in minutes)
MUPD_UP = 45    # Maximum usage per day for Up Motor (in minutes)

TI = 7  # Thread interval (in seconds)


todayDownUsage = 0
isDownMotorRunning = 0  # This must be 0 initally to start the motor in startMotor()

todayUpUsage = 0
isUpMotorRunning = 0

waterFilled = 0
waterSupply = 1
overheadTankFilled = 1

pastDay = -1
pastTime = 0  # past time

downMotorStartTime = 0
downMotorStopTime = 0

upMotorStartTime = 0
upMotorStopTime = 0

isStopMotorSignalRunning = 0

startNotificationSent = 0

# If log directory is not present in user's root, create it
if not os.path.exists(logDir):
    os.makedirs(logDir)

# Set configurations for logging
logging.basicConfig(
    filename = logDir + logFile,
    format = '%(asctime)s {%(levelname)s} %(message)s',
    level = logging.INFO
)


# Set pin numbering mode for GPIO pins
GPIO.setmode(GPIO.BOARD)

# Set the channels of GPIO pins
GPIO.setup(WSpin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(WFpin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(DOWN_MOTOR_PIN, GPIO.OUT)

GPIO.setup(OTFpin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(UP_MOTOR_PIN, GPIO.OUT)

# Initialize the output pins
GPIO.output(DOWN_MOTOR_PIN, 0)
GPIO.output(UP_MOTOR_PIN, 0)


# Telegram Bot Authorization Token
bot = telegram.Bot(AUTH_TOKEN)

# Send chat to Telegram users
def sendChat(message):
    global bot
    for user, chatId in USERS.items():
        try:
            bot.sendMessage(chat_id=chatId, text=message)
        except:
            time.sleep(1)


def printlog(level, input):
    if (DEBUG == 1):
        print(input)

    if (LOG == 1):
        if (level == "debug"):
            logging.debug(input)
        elif (level == "info"):
            logging.info(input)
        elif(level == "warning"):
            logging.warning(input)
        elif(level == "error"):
            logging.error(input)
        elif(level == "critical"):
            logging.critical(input)
        else:
            logging.info(input)



def startMotor():
    global isDownMotorRunning, todayDownUsage, downMotorStartTime, startNotificationSent
    if ((int(todayDownUsage/60) < MUPD_DOWN) and (waterFilled == 0)):
        if (isDownMotorRunning == 0):
            downMotorStartTime = int(time.time())
            isDownMotorRunning = 1
            printlog("info", "Down Motor started")

        if ((waterSupply == 1) and (isDownMotorRunning == 1) and (startNotificationSent == 0)):
            startNotificationSent = 1
            # Notify users that the motor has started. Also send todayDownUsage upto this point
            sendChat("Down Motor started " + u'\U0001f3b8' + "\n" + datetime.now().strftime("%d %b") + " usage: " + str(todayDownUsage // 3600 % 24) + " hrs, " + str(todayDownUsage // 60 % 60) + " min, " + str(todayDownUsage % 60) + " sec")


def stopMotor():
    global isDownMotorRunning, todayDownUsage, downMotorStartTime, startNotificationSent
    if (isDownMotorRunning == 1):
        downMotorStopTime = int(time.time())
        todayDownUsage = todayDownUsage + (downMotorStopTime - downMotorStartTime)
        isDownMotorRunning = 0
        printlog("info", "Down Motor stopped..." + str(todayDownUsage))

        if (startNotificationSent == 1):
            startNotificationSent = 0
            # Notify users that the motor has stopped. Also send todayDownUsage upto this point
            sendChat("Down Motor stopped " + u'\u26f1' + "\n" + datetime.now().strftime("%d %b") + " usage: " + str(todayDownUsage // 3600 % 24) + " hrs, " + str(todayDownUsage // 60 % 60) + " min, " + str(todayDownUsage % 60) + " sec")


def stopMotorSignal(signum, stack):
    global isStopMotorSignalRunning
    stopMotor()
    isStopMotorSignalRunning = 0


def stopMotorThread():
    global isStopMotorSignalRunning, isDownMotorRunning, waterSupply, waterFilled, todayDownUsage
    while (1):
        if ((isStopMotorSignalRunning == 0) and (isDownMotorRunning == 1) and (waterSupply == 0 or waterFilled == 1)):
            stopMotor()
            # Notify users that the motor has stopped. Also send todayDownUsage upto this point
            # sendChat("Down Motor stopped\n" + datetime.now().strftime("%d %b") + " usage: " + str(todayDownUsage // 3600 % 24) + " hrs, " + str(todayDownUsage // 60 % 60) + " min, " + str(todayDownUsage % 60) + " sec")
        time.sleep(TI)


def controlUpMotorThread():
    global isUpMotorRunning, todayUpUsage, overheadTankFilled
    while (1):
        if ((int(todayUpUsage/60) < MUPD_UP) and (overheadTankFilled == 0) and (isUpMotorRunning == 0)):
            isUpMotorRunning = 1
            upMotorStartTime = int(time.time())
            printlog("info", "Up Motor started")
            # Notify users that the motor has started. Also send todayUpUsage upto this point
            sendChat("Up Motor started " + u'\U0001f681' + "\n" + datetime.now().strftime("%d %b") + " usage: " + str(todayUpUsage // 3600 % 24) + " hrs, " + str(todayUpUsage // 60 % 60) + " min, " + str(todayUpUsage % 60) + " sec")

        if ((int(todayUpUsage/60) >= MUPD_UP) or (overheadTankFilled == 1)):
            if (isUpMotorRunning == 1):
                upMotorStopTime = int(time.time())
                todayUpUsage = todayUpUsage + (upMotorStopTime - upMotorStartTime)

                isUpMotorRunning = 0
                printlog("info", "Up Motor stopped..." + str(todayUpUsage))

                # Notify users that the motor has stopped. Also send todayUpUsage upto this point
                sendChat("Up Motor stopped " + u'\U0001f302' + "\n" + datetime.now().strftime("%d %b") + " usage: " + str(todayUpUsage // 3600 % 24) + " hrs, " + str(todayUpUsage // 60 % 60) + " min, " + str(todayUpUsage % 60) + " sec")
            isUpMotorRunning = 0

        time.sleep(TI)


def updateParameters():
    global waterSupply, waterFilled, overheadTankFilled
    while (1):
        totalPollCount = 20
        # Update waterSupply
        highCount = 0
        for x in range(0, totalPollCount):
            if (GPIO.input(WSpin)):
                highCount += 1
        if (highCount/totalPollCount > 0.75):
            waterSupply = 1
        else:
            waterSupply = 0

        # Update waterFilled
        highCount = 0
        for x in range(0, totalPollCount):
            if (GPIO.input(WFpin)):
                highCount += 1
        if (highCount/totalPollCount > 0.75):
            waterFilled = 1
        else:
            waterFilled = 0

        # Update overheadTankFilled
        highCount = 0
        for x in range(0, totalPollCount):
            if (GPIO.input(OTFpin)):
                highCount += 1
        if (highCount/totalPollCount > 0.75):
            overheadTankFilled = 1
        else:
            overheadTankFilled = 0

        time.sleep(TI)


if __name__ == "__main__":
    try:
        printlog("info", "****************************************************************************************************\n\n\n\n##################################################\n##################################################\n###                                            ###\n###  Home motor program is booting up........  ###\n###                                            ###\n##################################################\n##################################################\n\n\n")
        # Notify users about the program booting up
        sendChat("####################\n\nHome motor is booting up... " + u'\U0001f680' + "\n\n####################")


        # upt = Update Parameter Thread
        upt = threading.Thread(target = updateParameters)
        upt.start()

        time.sleep(2)

        # smt = Stop Motor Thread
        smt = threading.Thread(target = stopMotorThread)
        smt.start()

        # cumt = Control Up Motor Thread
        cumt = threading.Thread(target = controlUpMotorThread)
        cumt.start()


        while (1):
            now = datetime.now()
            currentTime = time.time() # in seconds
            nowDay = now.day # in day of month
            nowDecimalHour = now.hour + now.minute/100

            if (nowDay != pastDay):
                todayDownUsage = 0
                pastDay = nowDay

            if (waterSupply == 1):
                # Disable the alarm right now
                signal.alarm(0)
                isStopMotorSignalRunning = 0

                # If the water is not filled up...
                if (waterFilled == 0):
                    # Start the motor if not already running
                    startMotor()
            else:
                if ((currentTime > pastTime + TRI*60) and 
                    ((MST <= nowDecimalHour and nowDecimalHour <= MET) or 
                        (NST <= nowDecimalHour and nowDecimalHour <= NET))):

                    startMotor()
                    # Call stopMotorSignal in TRD seconds
                    signal.signal(signal.SIGALRM, stopMotorSignal)
                    signal.alarm(TRD)
                    isStopMotorSignalRunning = 1

                    # Set past time equal to current time
                    pastTime = currentTime

                # printlog("info", "Waiting..." + str(currentTime))

            # printlog("info", "Waiting..." + str(todayDownUsage))

            GPIO.output(DOWN_MOTOR_PIN, isDownMotorRunning)
            GPIO.output(UP_MOTOR_PIN, isUpMotorRunning)

            time.sleep(1)
    except:
        printlog("error", "Interrupted - Cleaning up...")
        GPIO.cleanup()





            



